package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"regexp"
	"strings"
	"time"
)

const baseURL = "https://api.clockify.me/api/v1/"
const configFilnamePattern = "%s.cfg"
const TimewarriorDir = ".timewarrior"

type Config struct {
	APIKey      string
	WorkSpaceID string
	LastEntry   time.Time
}

type TimeWarriorEntry struct {
	Start      string
	End        string
	Annotation string
	Tags       []string
}

type TimeEntryRequest struct {
	Start       string `json:"start"`
	End         string `json:"end,omitempty"`
	Task        string `json:"taskId,omitempty"`
	Project     string `json:"projectId,omitempty"`
	Description string `json:"description,omitempty"`
}

var debug = false
var verbose = false
var version string

func main() {
	var config Config
	newKey := flag.String("key", "", "Key to use to access Clockify API")
	workspace := flag.String("workspace", "", "Workspace name")
	init := flag.Bool("init", false, "Initialize a configuration file and exits")
	flag.Parse()
	homeDir, err := os.UserHomeDir()
	checkError(err)
	if len(homeDir) == 0 {
		checkError(errors.New("no valid home directory"))
	}
	executable, err := os.Executable()
	checkError(err)
	configPath := path.Join(homeDir, TimewarriorDir, fmt.Sprintf(configFilnamePattern, path.Base(executable)))
	configFile, err := os.Open(configPath)
	if err == nil {
		content, err := ioutil.ReadAll(configFile)
		checkError(err)
		err = json.Unmarshal(content, &config)
		checkError(err)
		defer configFile.Close()
	}
	if len(*newKey) > 0 {
		config.APIKey = *newKey
	} else {
		if len(config.APIKey) == 0 {
			checkError(errors.New("an API key is required"))
		}
	}

	if len(*workspace) != 0 {
		config.WorkSpaceID = getWorkSpaceID(config.APIKey, *workspace)
	}
	if !*init {
		projects := getProjects(config.APIKey, config.WorkSpaceID)
		r := parseReport()
		max := config.LastEntry
		for _, e := range r {
			timeEntry := convert(config.APIKey, config.WorkSpaceID, projects, e)
			if len(timeEntry.End) > 0 {
				jsonData, err := json.Marshal(timeEntry)
				checkError(err)
				if debug {
					fmt.Printf("Time entry: %s\n", e)
				}
				if len(timeEntry.Project) > 0 {
					endTime, err := time.Parse("2006-01-02T15:04:05Z", timeEntry.End)
					checkError(err)
					if endTime.After(config.LastEntry) {
						if pushEntry(config.APIKey, config.WorkSpaceID, jsonData) {
							if endTime.After(max) {
								max = endTime
							}
						} else {
							if verbose {
								fmt.Printf("Entry %v non pushed to Clockify\n", timeEntry)
							}
						}
					} else {
						if verbose {
							fmt.Printf("Entry ignore because ending %s before the time of the latest entry ended: %s\n", endTime.String(), max.String())
						}
					}
				}
			}
		}

		config.LastEntry = max
		if verbose || debug {
			fmt.Printf("verbose: %t\n", verbose)
			fmt.Printf("debug: %t\n", debug)
			fmt.Printf("version: %s\n", version)
		}
	}
	content, err := json.Marshal(config)
	checkError(err)
	ioutil.WriteFile(configPath, content, 0600)
}

func checkError(err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}
}

func getWorkSpaceID(key, name string) string {
	type WorkspaceDto struct {
		ID   string
		Name string
	}
	var id string
	client := &http.Client{}
	req, err := http.NewRequest("GET", getURL("workspaces").String(), nil)
	checkError(err)
	req.Header.Add("content-type", "application/json")
	req.Header.Add("X-Api-Key", key)
	resp, err := client.Do(req)
	checkError(err)
	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		fmt.Fprintf(os.Stderr, "failed to retrieve workspace; api returned status code %d\n", resp.StatusCode)
		if len(body) > 0 {
			fmt.Fprintf(os.Stderr, "%s\n", body)
		}
		os.Exit(1)
	}
	var workspaces []WorkspaceDto
	err = json.Unmarshal(body, &workspaces)
	checkError(err)
	for _, workspace := range workspaces {
		if strings.EqualFold(workspace.Name, name) {
			id = workspace.ID
			break
		}
	}
	if len(id) == 0 {
		checkError(errors.New(fmt.Sprintf("no workspace with name %s", name)))
	}
	return id
}

func getProjects(key, workspace string) map[string]string {
	type ProjectDtoImpl struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	}
	client := &http.Client{}
	req, err := http.NewRequest("GET", getURL(fmt.Sprintf("workspaces/%s/projects", workspace)).String(), nil)
	checkError(err)
	req.Header.Add("content-type", "application/json")
	req.Header.Add("X-Api-Key", key)
	resp, err := client.Do(req)
	checkError(err)
	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		fmt.Fprintf(os.Stderr, "failed to retrieve projects; api returned status code %d\n", resp.StatusCode)
		if len(body) > 0 {
			fmt.Fprintf(os.Stderr, "%s\n", body)
		}
		os.Exit(1)
	}
	var response []ProjectDtoImpl
	err = json.Unmarshal(body, &response)
	checkError(err)
	projects := make(map[string]string)
	for _, project := range response {
		name := strings.ToLower(project.Name)
		projects[name] = project.ID
	}
	return projects
}

func getURL(endpoint string) *url.URL {
	u, err := url.Parse(baseURL)
	checkError(err)
	u.Path = path.Join(u.Path, endpoint)
	return u
}

func getTasks(key, project, workspace string) map[string]string {
	type TaskDto struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	}
	client := &http.Client{}
	url := getURL(fmt.Sprintf("workspaces/%s/projects/%s/project/tasks", workspace, project))
	req, err := http.NewRequest("GET", url.String(), nil)
	checkError(err)
	req.Header.Add("content-type", "application/json")
	req.Header.Add("X-Api-Key", key)
	resp, err := client.Do(req)
	checkError(err)
	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		fmt.Fprintf(os.Stderr, "failed to retrieve tasks; api returned status code %d\n", resp.StatusCode)
		if len(body) > 0 {
			fmt.Fprintf(os.Stderr, "%s\n", body)
		}
		os.Exit(1)
	}
	var response []TaskDto
	err = json.Unmarshal(body, &response)
	checkError(err)
	tasks := make(map[string]string)
	for _, task := range response {
		name := strings.ToLower(task.Name)
		tasks[name] = task.ID
	}
	return tasks
}

func getTask(key, project, workspace, task string) string {
	var id string
	type TaskDto struct {
		ID string `json:"id"`
	}
	client := &http.Client{}
	u := getURL(fmt.Sprintf("workspaces/%s/projects/%s/project/tasks", workspace, project))
	v := url.Values{}
	v.Add("name", task)
	v.Add("is-active", "true")
	u.RawQuery = v.Encode()
	req, err := http.NewRequest("GET", u.String(), nil)
	checkError(err)
	req.Header.Add("content-type", "application/json")
	req.Header.Add("X-Api-Key", key)
	resp, err := client.Do(req)
	checkError(err)
	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		if resp.StatusCode != http.StatusNotFound {
			fmt.Fprintf(os.Stderr, "failed to retrieve tasks; api returned status code %d\n", resp.StatusCode)
			if len(body) > 0 {
				fmt.Fprintf(os.Stderr, "%s\n", body)
			}
			os.Exit(1)
		}
		return id
	}
	var response []TaskDto
	err = json.Unmarshal(body, &response)
	checkError(err)

	for _, task := range response {
		id = task.ID
		break
	}
	return id
}

func pushEntry(key, workspace string, body []byte) bool {
	client := &http.Client{}
	u := getURL(fmt.Sprintf("workspaces/%s/time-entries", workspace))
	req, err := http.NewRequest("POST", u.String(), bytes.NewBuffer(body))
	checkError(err)
	req.Header.Add("content-type", "application/json")
	req.Header.Add("X-Api-Key", key)
	resp, err := client.Do(req)
	if debug {
		fmt.Printf("Sent %q to %s\n", body, u)
	}
	checkError(err)
	body, err = ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusCreated {
		fmt.Printf("Clockify responded with status: %d(%s)\n", resp.StatusCode, resp.Status)
		var payload map[string]interface{}
		err = json.Unmarshal(body, &payload)
		if err != nil {
			fmt.Printf("Error parsing response body: %s", err.Error())
		} else {
			fmt.Printf("Response body: %q\n", payload)
		}
		return false
	}
	return true
}

func parseReport() []TimeWarriorEntry {
	var r []TimeWarriorEntry
	scanner := bufio.NewScanner(os.Stdin)
	var b strings.Builder
	for scanner.Scan() {
		fmt.Fprintf(&b, scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		checkError(err)
	}
	text := b.String()
	debugRe := buildRe("debug")
	verboseRe := buildRe("verbose")
	if debugRe.MatchString(text) {
		debug = true
	}
	if verboseRe.MatchString(text) {
		verbose = true
	}

	VersionRe := regexp.MustCompile("temp\\.version:\\s+([[:digit:]\\.]+)")
	submatch := VersionRe.FindStringSubmatch(text)
	if submatch != nil {
		version = submatch[1]
	}

	jsonStart := strings.IndexByte(text, '[')
	jsonText := text[jsonStart:]
	err := json.Unmarshal([]byte(jsonText), &r)
	checkError(err)
	return r
}

func buildRe(setting string) *regexp.Regexp {
	trueValues := "on|1|yes|y|true"
	return regexp.MustCompile(fmt.Sprintf("%s:\\s+(%s)", setting, trueValues))
}

func formatTimeStamp(timestamp string) string {
	re := regexp.MustCompile(`([0-9]{4})([0-9]{2})([0-9]{2}T)([0-9]{2})([0-9]{2})([0-9]{2}Z)`)
	time := re.ReplaceAllString(timestamp, "$1-$2-$3$4:$5:$6")
	return time
}

func convert(key, workspace string, projects map[string]string, twEntry TimeWarriorEntry) TimeEntryRequest {
	er := TimeEntryRequest{
		Start:       formatTimeStamp(twEntry.Start),
		End:         formatTimeStamp(twEntry.End),
		Description: twEntry.Annotation,
	}
	tags := twEntry.Tags
	// The first tag matching a project name we use
	for _, tag := range tags {
		name := strings.ToLower(tag)
		for pName, pID := range projects {
			if pName == name {
				er.Project = pID
				break
			}
		}
		/*
		if id, ok := projects[name]; ok {
			er.Project = id
			//tags = append(tags[:i], tags[i+1:]...) // We can safely do this, because we break up afterwards
			break
		}

		 */
	}
	// Next, if we have a project, we try to fill up the tasks
	/*
	if len(er.Project) > 0 {
		for _, tag := range tags {
			id := getTask(key, er.Project, workspace, tag)
			if len(id) > 0 && len(er.Task) == 0 {
				er.Task = id
			}
		}
	}

	 */
	if len(er.Project) == 0 {
		log.Fatal("No suitable project found")
	}
	return er
}